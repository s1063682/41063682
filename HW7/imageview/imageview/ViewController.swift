//
//  ViewController.swift
//  imageview
//
//  Created by apple on 2019/11/12.
//  Copyright © 2019 pu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var imageshow: UIImageView!
    var i = 3
    override func viewDidLoad() {
        super.viewDidLoad()
        imageshow.image = UIImage(named: "IMG_3.jag")
        // Do any additional setup after loading the view.
    }

    @IBAction func preview(_ sender: Any) {
        i -= 1
        if(i == 0){
            i = 3
        }
        let a = "IMG_" + String(i) + ".jag"
        imageshow.image = UIImage(named: a)
    }
    
    @IBAction func nextview(_ sender: Any) {
        i += 1
        if(i == 4){
            i = 1
        }
        let a = "IMG_" + String(i) + ".jag"
        imageshow.image = UIImage(named: a)
    }
}

