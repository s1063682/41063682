//
//  ViewController.swift
//  datepicker
//
//  Created by apple on 2019/11/12.
//  Copyright © 2019 pu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var dateFormatter = DateFormatter()
    
    @IBOutlet weak var la: UILabel!
    @IBOutlet weak var datepicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datepicker.datePickerMode = UIDatePicker.Mode.dateAndTime
        datepicker.locale = Locale.init(identifier: "zh_TW")
        datepicker.date = Date()
        dateFormatter.dateFormat = "西元y年 M月 d日 hh時 mm分 ss秒"
        la.text = dateFormatter.string(from: datepicker.date)
        // Do any additional setup after loading the view.
    }

    @IBAction func timeChange(_ sender: Any) {
        la.text = dateFormatter.string(from: datepicker.date)
    }
    
}

