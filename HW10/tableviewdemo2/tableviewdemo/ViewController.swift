//
//  ViewController.swift
//  tableviewdemo
//
//  Created by apple on 2019/11/26.
//  Copyright © 2019 pu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var lab: UILabel!
    @IBOutlet weak var tableview: UITableView!
    var balls:Array<Dictionary<String,String>> = [["name":"籃球", "value":"500", "imageName":"basketball"],
                                                  ["name":"足球", "value":"300", "imageName":"football"],
                                                  ["name":"棒球", "value":"400", "imageName":"baseball"],
                                                  ["name":"其他", "value":"100", "imageName":"other"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableview.delegate = self
        self.tableview.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return balls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        var ball:Dictionary<String:String> = balls[indexPath.row]
        cell.textLabel!.text = ball["name"]
        cell.detialTextLabel!.text = ball["value"]
        cell.imageView!.image = UIImage(name:ball["imageName"]!)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        lab.text = "你like" + balls[indexPath.row]
    }
}


