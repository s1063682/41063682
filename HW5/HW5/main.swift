import Foundation

class Psersonal {
    var name:String
    var height:Double
    var weight:Double
    
    init(name:String, height:Double, weight:Double){
        self.name = name
        self.height = height
        self.weight = weight
    }
    
    func bmi() -> Double{
        return weight / (height * height)
    }
    func show(){
        print("姓名：\(name)")
        print("身高：\(height)")
        print("體重：\(weight)")
    }
}

class Student:Psersonal{
    var score:[Double]? = nil
    init(name: String, height: Double, weight: Double, score:Double...) {
        super.init(name: name, height: height, weight: weight)
        self.score = score
    }
    func average() -> Double{
        var sum:Double = 0.0
        for a in score!{
            sum = sum + a
        }
        sum = sum / Double(score!.count)
        return sum
    }
    override func show(){
        print("姓名：\(name)")
        print("身高：\(height)")
        print("體重：\(weight)")
        print("國文：\(score![0])")
        print("英文：\(score![1])")
        print("數學：\(score![2])")
        print("BMI：\(bmi())")
        print("各科平均：\(average())")
    }
}

var rect = Student(name: "??", height: 1.68, weight: 75.0, score:85.0,60.0,90.0)
rect.show()
