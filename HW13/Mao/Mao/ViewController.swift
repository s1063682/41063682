//
//  ViewController.swift
//  Mao
//
//  Created by apple on 2019/12/24.
//  Copyright © 2019 pu. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController {

    @IBOutlet weak var map: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let anns = [MKPointAnnotation(), MKPointAnnotation()]
        anns[0].coordinate = CLLocationCoordinate2DMake(24.137426, 121.275753)
        anns[0].title = "武嶺"
        anns[0].subtitle = "南投縣仁愛鄉"
        
        anns[0].coordinate = CLLocationCoordinate2DMake(23.510041, 120.700458)
        anns[0].title = "奮起湖"
        anns[0].subtitle = "嘉義縣竹崎鄉"
        
        map.addAnnotations(anns)
        map.setCenter(anns[0].coordinate, animated: false)
        
    }


}

