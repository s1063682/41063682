//
//  ViewController.swift
//  AutoLayout
//
//  Created by apple on 2019/11/19.
//  Copyright © 2019 pu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let label  = UILabel()
        label.backgroundColor = UIColor.lightGray
        label.text = "Hello"
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        
        label.heightAnchor.constraint(equalToConstant: 22).isActive = true
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        label.topAnchor.constraint(equalTo: view.topAnchor, constant: 40).isActive = true
        label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        // Do any additional setup after loading the view.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mysegue"{
            let vc = segue.destination as! ViewController2
            vc.str2 = "hello"
        }
    }

}

