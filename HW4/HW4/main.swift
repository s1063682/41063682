import Foundation

func Grade(n: Int) -> String{
    var str = ""
    if(n < 60){
        str = "不及格"
    }else if(n < 70){
        str = "及格"
    }else if(n <= 80){
        str = "中等"
    }else if(n < 90){
        str = "良好"
    }else{
        str = "優秀"
    }
    return str
}

var grade = [95,85,80,65,59]
print("成績\t 顯示")
for i in 0...4{
    print("\(grade[i])\t",Grade(n:grade[i]))
}
