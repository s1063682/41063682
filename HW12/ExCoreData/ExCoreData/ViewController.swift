//
//  ViewController.swift
//  ExCoreData
//
//  Created by apple on 2019/12/17.
//  Copyright © 2019 pu. All rights reserved.
//

import UIKit
import CoreData
class ViewController: UIViewController {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Student")
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnInsert(_ sender: Any) {
        let student = NSEntityDescription.insertNewObject(forEntityName: "Student", into: context) as! Student
        student.id = 1
        student.name = "Urua"
        student.height = 74.0
        do{
            try context.save()
            print("Saved")
        }catch{
            print("Save Error")
        }
    }
    
    @IBAction func btnsearch(_ sender: Any) {
        do{
            let results = try context.fetch(request) as! [Student]
            for result in results {
                print("\(result.id) \(result.name!) \(result.height)")
            }
        }catch{
            print("Search error")
        }
    }
    
    @IBAction func btnchange(_ sender: Any) {
        do{
            let results = try context.fetch(request) as! [Student]
            for result in results {
                if(result.id == 1){
                    result.id = 3
                    result.name = "Urua3"
                    result.height = 74.0
                    try context.save()
                }
            }
        }catch{
            print("Edit error")
        }    }
    @IBAction func btndelete(_ sender: Any) {
        do{
            let results = try context.fetch(request) as! [Student]
            for result in results {
                if(result.id == 3){
                    print("\(result.name!) is deletes")
                    context.delete(result)
                }
            }
        }catch{
            print("Delete error")
        }    }
}

