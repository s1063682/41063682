//
//  ViewController.swift
//  tableviewdemo
//
//  Created by apple on 2019/11/26.
//  Copyright © 2019 pu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var btn_switch: UISwitch!
    @IBOutlet weak var labball: UITextField!
    @IBOutlet weak var lab: UILabel!
    @IBOutlet weak var tableview: UITableView!
    var balls:Array<Dictionary<String,String>> = [["name":"籃球", "value":"500", "imageName":"basketball"],
                                                  ["name":"足球", "value":"300", "imageName":"football"],
                                                  ["name":"棒球", "value":"400", "imageName":"baseball"],
                                                  ["name":"其他", "value":"100", "imageName":"other"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableview.delegate = self
        self.tableview.dataSource = self
        //tableview.setEditing(false, animated: false)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return balls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableview.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        var ball:Dictionary<String,String> = balls[indexPath.row]
        cell.textLabel!.text = ball["name"]
        cell.detailTextLabel!.text = ball["value"]
        cell.imageView!.image = UIImage(named:ball["imageName"]!)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var ball:Dictionary<String,String> = balls[indexPath.row]
        lab.text = "你like" + ball["name"]! + "$\(ball["value"]!)"
    }
    @IBAction func btninsert(_ sender: Any) {
        let ball = ["name":"\(labball.text!)","value":"500", "imageName":"other"]
        balls.append(ball)
        tableview.reloadData()
    }
    //can del
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.delete
    }
    //del
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        balls.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.bottom)
    }
    
    @IBAction func btniswttch(_ sender: Any) {
        if(btn_switch.isOn){
            tableview.setEditing(true, animated: true)
        }else{
            tableview.setEditing(false, animated: false)
        }
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        var myball:Dictionary<String,String> = balls[sourceIndexPath.row]
        balls[sourceIndexPath.row] = balls[destinationIndexPath.row]
        balls[destinationIndexPath.row] = myball
    }
}

